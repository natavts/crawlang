package itmo.escience
        
class PostContext(val awf: AWF, val previousNode: OperationTypes) extends NodesContext {
    def timestamp(): TimestampContext = {
        val op = new Timestamp()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new TimestampContext(awf, op)
      }
        
    def likes(): LikeContext = {
        val op = new Likes()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new LikeContext(awf, op)
      }
        
    def location(): LocationContext = {
        val op = new Location()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new LocationContext(awf, op)
      }
        
    def text(): TextContext = {
        val op = new Text()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new TextContext(awf, op)
      }
        
    def user(): UserContext = {
        val op = new User()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new UserContext(awf, op)
      }
        
    def comments(): CommentContext = {
        val op = new Comments()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new CommentContext(awf, op)
      }
        
    def apply(func:Function[PostContext, _ <: NodesContext]) = {
    
        val extendedAwf = func(this)
        
        new PostContext(extendedAwf.awf, this.previousNode)
      }
}
        