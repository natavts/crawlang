package itmo.escience
        
class CommentContext(val awf: AWF, val previousNode: OperationTypes) extends NodesContext {
    def text(): TextContext = {
        val op = new Text()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new TextContext(awf, op)
      }
        
    def user(): UserContext = {
        val op = new User()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new UserContext(awf, op)
      }
        
    def likes(): LikeContext = {
        val op = new Likes()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new LikeContext(awf, op)
      }
        
    def timestamp(): TimestampContext = {
        val op = new Timestamp()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new TimestampContext(awf, op)
      }
        
    def apply(func:Function[CommentContext, _ <: NodesContext]) = {
    
        val extendedAwf = func(this)
        
        new CommentContext(extendedAwf.awf, this.previousNode)
      }
}
        