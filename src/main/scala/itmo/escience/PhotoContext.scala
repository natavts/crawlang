package itmo.escience
        
class PhotoContext(val awf: AWF, val previousNode: OperationTypes) extends NodesContext {
    def timestamp(): TimestampContext = {
        val op = new Timestamp()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new TimestampContext(awf, op)
      }
        
    def likes(): LikeContext = {
        val op = new Likes()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new LikeContext(awf, op)
      }
        
    def image(): ImageContext = {
        val op = new Image()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new ImageContext(awf, op)
      }
        
    def user(): UserContext = {
        val op = new User()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new UserContext(awf, op)
      }
        
    def comments(): CommentContext = {
        val op = new Comments()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new CommentContext(awf, op)
      }
        
    def text(): TextContext = {
        val op = new Text()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new TextContext(awf, op)
      }
        
    def location(): LocationContext = {
        val op = new Location()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new LocationContext(awf, op)
      }
        
    def apply(func:Function[PhotoContext, _ <: NodesContext]) = {
    
        val extendedAwf = func(this)
        
        new PhotoContext(extendedAwf.awf, this.previousNode)
      }
}
        