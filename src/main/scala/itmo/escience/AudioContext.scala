package itmo.escience
        
class AudioContext(val awf: AWF, val previousNode: OperationTypes) extends NodesContext {
    def text(): TextContext = {
        val op = new Text()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new TextContext(awf, op)
      }
        
    def sound(): SoundContext = {
        val op = new Sound()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new SoundContext(awf, op)
      }
        
    def apply(func:Function[AudioContext, _ <: NodesContext]) = {
    
        val extendedAwf = func(this)
        
        new AudioContext(extendedAwf.awf, this.previousNode)
      }
}
        