package itmo.escience
        
class LikeContext(val awf: AWF, val previousNode: OperationTypes) extends NodesContext {
    def apply(func:Function[LikeContext, _ <: NodesContext]) = {
    
        val extendedAwf = func(this)
        
        new LikeContext(extendedAwf.awf, this.previousNode)
      }
}
        