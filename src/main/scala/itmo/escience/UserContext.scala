package itmo.escience
        
class UserContext(val awf: AWF, val previousNode: OperationTypes) extends NodesContext {
    def posts(): PostContext = {
        val op = new Posts()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new PostContext(awf, op)
      }
        
    def text(): TextContext = {
        val op = new Text()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new TextContext(awf, op)
      }
        
    def location(): LocationContext = {
        val op = new Location()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new LocationContext(awf, op)
      }
        
    def photos(): PhotoContext = {
        val op = new Photos()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new PhotoContext(awf, op)
      }
        
    def friends(): UserContext = {
        val op = new Friends()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new UserContext(awf, op)
      }
        
    def followed(): UserContext = {
        val op = new Followed()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new UserContext(awf, op)
      }
        
    def audios(): AudioContext = {
        val op = new Audios()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new AudioContext(awf, op)
      }
        
    def post(): PostContext = {
        val op = new Post()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new PostContext(awf, op)
      }
        
    def followers(): UserContext = {
        val op = new Followers()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new UserContext(awf, op)
      }
        
    def videos(): VideoContext = {
        val op = new Videos()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new VideoContext(awf, op)
      }
        
    def apply(func:Function[UserContext, _ <: NodesContext]) = {
    
        val extendedAwf = func(this)
        
        new UserContext(extendedAwf.awf, this.previousNode)
      }
}
        