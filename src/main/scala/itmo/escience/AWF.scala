package itmo.escience

import javax.annotation.PostConstruct

import org.jgrapht.graph.{DefaultDirectedGraph, DefaultEdge}

/**
  * Created by user on 04.04.2016.
  */
class AWF extends DefaultDirectedGraph[OperationTypes, DefaultEdge](classOf[DefaultEdge]) {}


//class AWF[N, E[X] <: EdgeLikeIn[X]] extends Graph[N, E]{
//  override def empty: Graph[N, E] = ???
//
//  override implicit val edgeManifest: Manifest[E[N]] = _
//
//  override implicit def config: graphCompanion.Config with AWF.this.type = ???
//
//  override def -?(node: N): Graph[N, E] = ???
//
//  override protected def +#(edge: E[N]): Graph[N, E] = ???
//
//  override def +(node: N): Graph[N, E] = ???
//
//  override protected def -#(edge: E[N]): Graph[N, E] = ???
//
//  override def -(node: N): Graph[N, E] = ???
//
//  override protected def -!#(edge: E[N]): Graph[N, E] = ???
//
//  override type Config = this.type
//  override type NodeT = this.type
//  override type EdgeSetT = this.type
//  override type NodeSetT = this.type
//  override val graphCompanion: GraphCompanion[Graph] = _
//
//  override def newTraversal(direction: Direction, nodeFilter: (AWF.this.type) => Boolean, edgeFilter: (AWF.this.type) => Boolean, nodeVisitor: (AWF.this.type) => VisitorReturn, edgeVisitor: (AWF.this.type) => Unit, ordering: ElemOrdering): Traversal = ???
//
//  override def findCycle(nodeFilter: (AWF.this.type) => Boolean, edgeFilter: (AWF.this.type) => Boolean, maxDepth: Int, nodeVisitor: (AWF.this.type) => VisitorReturn, edgeVisitor: (AWF.this.type) => Unit, ordering: ElemOrdering): Option[Cycle] = ???
//
//  override def nodes: AWF.this.type = ???
//
//  override def edges: AWF.this.type = ???
//
//  override protected def newNode(n: N): AWF.this.type = ???
//
//  override protected def newEdge(innerEdge: E[AWF.this.type]): AWF.this.type = ???
//
//  override type EdgeT = this.type
//}


//class Node(val name:String)
//case object BeginNode extends Node("begin")
//case object EndNode   extends Node("end")

object Network {
  def apply(s: String) = {
    val networkNode = new NetworkNode()
    val awf = new AWF
    awf.addVertex(networkNode)
    new NetworkContext(awf, networkNode)
  }
}


trait OperationTypes


trait BasePostOps extends OperationTypes

class Likes() extends BasePostOps

class PostDate() extends BasePostOps

class PostLocations() extends BasePostOps

class Text() extends BasePostOps

class User() extends BasePostOps

class Video() extends BasePostOps

class Picture() extends BasePostOps

class NetworkNode() extends OperationTypes


trait ServiceOps extends OperationTypes

class FilterOp extends ServiceOps
class Sync extends ServiceOps


trait NetworkOperations extends OperationTypes

class Posts(list_id: List[_]) extends NetworkOperations

class Users(list_id: List[_]) extends NetworkOperations

class Locations(list_id: List[String]) extends NetworkOperations


trait PostOperations extends BasePostOps

class Comments() extends PostOperations

class Reposts() extends PostOperations

class PostText() extends PostOperations


trait UserOperations extends OperationTypes

class UserPosts() extends UserOperations

class Followers() extends UserOperations

class Followed() extends UserOperations

class Friends() extends UserOperations

class UserLocation() extends UserOperations

class Name() extends UserOperations

class Bio() extends UserOperations

class Birth() extends UserOperations

class CommentOperations extends BasePostOps


trait LocationOperations extends OperationTypes

class Geo() extends LocationOperations

class GeoName() extends LocationOperations

class Count() extends OperationTypes

class LikedUser extends OperationTypes


trait BooleanOperations extends OperationTypes

class TextContains(s: String) extends BooleanOperations

class Search() extends OperationTypes
class Streaming() extends OperationTypes {}

trait Context {
  def awf:AWF

  def filter[T](newContext:T, func:( T => BooleanContext), clazz:Class[T]) = {
    val filteredContext:BooleanContext = func(newContext)

    val fop = new FilterOp()
    filteredContext.awf.addVertex(fop)
    filteredContext.awf.addEdge(filteredContext.previousNode, fop)

    val ctr = clazz.getConstructor(classOf[AWF], classOf[OperationTypes])
    ctr.newInstance(filteredContext.awf, fop)
  }
  def context(): Unit = {

  }

  def sync = {

  }
}

trait NodesContext extends Context

trait ValueContext extends Context

class NetworkContext(val awf: AWF, val previousNode: OperationTypes) extends Context {

  //  def posts(func:Function[PostsContext, BooleanContext]): PostsContext = {
  //    val op = new Posts(List())
  //    awf.addVertex(op)
  //    awf.addEdge(previousNode, op)
  //    val newContext = new PostsContext(awf, op)
  //
  //    val extendedAwf:Context = func(newContext)
  //
  //    new PostsContext(extendedAwf.awf, op)
  //  }


  def posts(func:(PostsContext => BooleanContext)): PostsContext = {
    val op = new Posts(List())
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    val newContext = new PostsContext(awf, op)

    filter(newContext, func, classOf[PostsContext])
  }


  def posts(list_ids: List[String]): PostsContext = {
    val op = new Posts(list_ids)
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new PostsContext(awf, op)
  }

  def posts(): PostsContext = {
    val op = new Posts(List())
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new PostsContext(awf, op)
  }

  def users(list_ids: List[_]): UsersContext = {
    val op = new Users(list_ids)
    awf.addVertex(op)
    awf.addEdge(previousNode, op)


    //this.posts((p:PostsContext) => new BooleanContext(awf, null))

    new UsersContext(awf, op)
  }

  def locations(list_ids: List[String]): LocationsContext = {
    val op = new Locations(list_ids)
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new LocationsContext(awf, op)
  }


  def search(func:(NetworkContext => BooleanContext)): NetworkContext = {
    val op = new Search()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    val newContext = new NetworkContext(awf, op)

    filter(newContext, func, classOf[NetworkContext])
  }

  def listen(duration: Int): Unit ={
    new Streaming()
  }

//  def search(func:(SearchContext => BooleanContext)): SearchContext = {
//    val op = new Search()
//    awf.addVertex(op)
//    awf.addEdge(previousNode, op)
//    val newContext = new SearchContext(awf, op)
//
//    filter(newContext, func, classOf[SearchContext])
//  }


}

//class SearchContext(val awf:AWF, val previousNode:OperationTypes) extends NodesContext

class BooleanContext(val awf:AWF, val previousNode:OperationTypes) extends Context {
}

class LocationsContext(val awf: AWF, val previousNode: OperationTypes) extends Context {
  def geo(): GeoContext = {
    val op = new Geo()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new GeoContext(awf, op)
  }

  def name(): TextContext = {
    val op = new GeoName()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new TextContext(awf, op)
  }

}

trait BasePostMethods extends NodesContext {
  val awf: AWF
  val previousNode: OperationTypes

  def user(): UsersContext = {
    val op = new User()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new UsersContext(awf, op)
  }

  def picture(): PictureContext = {
    val op = new Picture()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new PictureContext(awf, op)
  }

  def postDate(): TimestampContext = {
    val op = new PostDate()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new TimestampContext(awf, op)
  }

  def location(): GeoContext = {
    val op = new PostLocations()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new GeoContext(awf, op)
  }

  def text(): TextContext = {
    val op = new PostText()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new TextContext(awf, op)
  }

  def video(): VideoContext = {
    val op = new Video()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new VideoContext(awf, op)
  }
}


class PostsContext(val awf: AWF, val previousNode: OperationTypes) extends BasePostMethods {
  def comments(): CommentsContext = {
    val op = new Comments()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new CommentsContext(awf, op)
  }

  def reposts(): PostsContext = {
    val op = new Reposts()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new PostsContext(awf, op)
  }

  def likes(): LikesContext = {
    val op = new Likes()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new LikesContext(awf, op)
  }

  def apply(func:Function[PostsContext, _ <: NodesContext]) = {

    val extendedAwf = func(this)

    new PostsContext(extendedAwf.awf, this.previousNode)
  }

//  def search(func:(NetworkContext => BooleanContext)): NetworkContext = {
//    val op = new Search()
//    awf.addVertex(op)
//    awf.addEdge(previousNode, op)
//    val newContext = new NetworkContext(awf, op)
//
//    filter(newContext, func, classOf[NetworkContext])
//  }

  def listen(duration: Int): Unit ={
    def to(func:(PostsContext => BooleanContext)): PostsContext = {
      val op = new Streaming()
      awf.addVertex(op)
      awf.addEdge(previousNode, op)
      val newContext = new PostsContext(awf, op)

      filter(newContext, func, classOf[PostsContext])
    }
  }



}

class CommentsContext(val awf: AWF, val previousNode: OperationTypes) extends NodesContext {

}

class UsersContext(val awf: AWF, val previousNode: OperationTypes) extends NodesContext {
  def posts(): PostsContext = {
    val op = new UserPosts()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new PostsContext(awf, op)
  }

  def followers(): UsersContext = {
    val op = new Followers()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new UsersContext(awf, op)
  }

  def followed(): UsersContext = {
    val op = new Followed()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new UsersContext(awf, op)
  }

  def friends(): UsersContext = {
    val op = new Friends()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new UsersContext(awf, op)
  }

  def location(): LocationsContext = {
    val op = new UserLocation()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new LocationsContext(awf, op)
  }

  def bio(): TextContext = {
    val op = new Bio()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new TextContext(awf, op)
  }

  def birth(): TimestampContext = {
    val op = new Birth()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new TimestampContext(awf, op)
  }

  def name(): TextContext = {
    val op = new Name()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new TextContext(awf, op)
  }

  def listen(duration: Int): Unit ={
    def to(func:(PostsContext => BooleanContext)): PostsContext = {
      val op = new Streaming()
      awf.addVertex(op)
      awf.addEdge(previousNode, op)
      val newContext = new PostsContext(awf, op)

      filter(newContext, func, classOf[PostsContext])
    }
  }



}

class LikesContext(val awf: AWF, val previousNode: OperationTypes) extends ValueContext {
  def count(): CountContext = {
    val op = new Count()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new CountContext(awf, op)
  }

  def users(): UsersContext = {
    val op = new LikedUser()
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new UsersContext(awf, op)
  }

}

class AudioContext(val awf: AWF, val previousNode: OperationTypes) extends ValueContext {}

class ContactContext(val awf: AWF, val previousNode: OperationTypes) extends ValueContext {}

class CountContext(val awf: AWF, val previousNode: OperationTypes) extends ValueContext {}

class GeoContext(val awf: AWF, val previousNode: OperationTypes) extends ValueContext {}

class PictureContext(val awf: AWF, val previousNode: OperationTypes) extends ValueContext {}

class TextContext(val awf: AWF, val previousNode: OperationTypes) extends ValueContext {
  def contains(s: String): BooleanContext ={
    val op = new TextContains(s)
    awf.addVertex(op)
    awf.addEdge(previousNode, op)
    new BooleanContext(awf, op)
  }
}

class TimestampContext(val awf: AWF, val previousNode: OperationTypes) extends ValueContext {}

class VideoContext(val awf: AWF, val previousNode: OperationTypes) extends ValueContext {}
