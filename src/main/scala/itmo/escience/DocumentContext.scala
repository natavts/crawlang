package itmo.escience
        
class DocumentContext(val awf: AWF, val previousNode: OperationTypes) extends NodesContext {
    def apply(func:Function[DocumentContext, _ <: NodesContext]) = {
    
        val extendedAwf = func(this)
        
        new DocumentContext(extendedAwf.awf, this.previousNode)
      }
}
        