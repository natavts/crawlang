import itmo.escience.{Network}
import org.scalatest.FlatSpec

/**
  * Created by user on 04.04.2016.
  */

class NetworkSpec extends FlatSpec {

  "Network" should "be able to build awf to return posts" in {
    val ids:List[String] = List("1", "2")
    val posts = Network("VK.COM").posts(ids)
    val awf = posts.awf
    assert(awf.vertexSet().size() == 2)
//    assert(awf.nodes.get(0) == BeginNode)
//    assert(awf.nodes.get(1).name == "postsOp")
//    assert(awf.nodes.get(2) == EndNode)
  }

  "Network" should "be able to build awf to return posts of user,s friends" in {
    val ids:List[String] = List("1")
    val posts = Network("VK.COM").users(ids).friends().posts()
    val awf = posts.awf
    assert(awf.vertexSet().size() == 4)
    //    assert(awf.nodes.get(0) == BeginNode)
    //    assert(awf.nodes.get(1).name == "postsOp")
    //    assert(awf.nodes.get(2) == EndNode)
  }

}
