import xml.etree.ElementTree as etree
import lxml
import os

#    <Declaration>
#        <NamedIndividual IRI="#getInstagrammVideo"/>
#    </Declaration>
#
#    <ClassAssertion>
#        <Class IRI="#VideosOp"/>
#        <NamedIndividual IRI="#getInstagrammVideo"/>
#    </ClassAssertion>
#
#    <ObjectPropertyAssertion>
#        <ObjectProperty abbreviatedIRI="owl:implementedFor"/>
#        <NamedIndividual IRI="#getInstagrammVideo"/>
#        <NamedIndividual abbreviatedIRI="owl:Instagram"/>
#    </ObjectPropertyAssertion>

networks = ["vk", "instagram", "twitter"]
tree = lxml.etree.parse(os.getcwd() + '/tmp.xml')
root = tree.getroot()
searchOps = {}



def getAttrVal(el):
    key = list(el.keys())[0]
    return el[key][1:]

def declaration(name):
    decl = lxml.etree.SubElement(root, 'Declaration')
    lxml.etree.SubElement( decl, 'NamedIndividual', attrib={'IRI': '#' + name})
    pass

def classAssertion(opname, name):
    clsass = lxml.etree.SubElement(root, 'ClassAssertion')
    lxml.etree.SubElement( clsass, 'Class', attrib={'IRI': '#' + opname})
    lxml.etree.SubElement( clsass, 'NamedIndividual', attrib={'IRI': '#' + name})
    if opname in list(searchOps.keys()):
        searchOps[opname]["individuals"].append(name)
    else:
        searchOps[opname] = {"individuals": [name]}
    pass

def objectPropertyAssertion(n, opname, ctxname, name):
    opa = lxml.etree.SubElement(root, 'ObjectPropertyAssertion')
    lxml.etree.SubElement( opa, 'ObjectProperty', attrib={'IRI': '#implementedFor'})
    lxml.etree.SubElement( opa, 'NamedIndividual', attrib={'IRI': '#' + name})
    lxml.etree.SubElement( opa, 'NamedIndividual', attrib={'IRI': '#' + n})

    opa = lxml.etree.SubElement(root, 'ObjectPropertyAssertion')
    lxml.etree.SubElement( opa, 'ObjectProperty', attrib={'IRI': '#isImplementedOn'})
    lxml.etree.SubElement( opa, 'NamedIndividual', attrib={'IRI': '#' + name})
    lxml.etree.SubElement( opa, 'NamedIndividual', attrib={'IRI': '#' + ctxname})

def objectPropertyReturns(opname, ctxname, name):
    opa = lxml.etree.SubElement(root, 'ObjectPropertyAssertion')
    lxml.etree.SubElement( opa, 'ObjectProperty', attrib={'IRI': '#returns'})
    lxml.etree.SubElement( opa, 'NamedIndividual', attrib={'IRI': '#' + name})
    lxml.etree.SubElement( opa, 'NamedIndividual', attrib={'IRI': '#' + ctxname})


def makeOperations(opname, ctxname):
    for n in networks:
        name = n + ctxname[:-7].capitalize() + opname
        declaration(name)
        classAssertion(opname, name)
        objectPropertyAssertion(n, opname, ctxname, name)

if __name__ == "main":

    subclasses = root.findall('{http://www.w3.org/2002/07/owl#}SubClassOf')
    ops = []
    for el in subclasses:
        if 'IRI' in el[0].attrib:
            if el[1].attrib["IRI"] == "#SearchOp":
                ops.append(getAttrVal(el[0].attrib))

    objectPropertyAssertions = root.findall('{http://www.w3.org/2002/07/owl#}ObjectPropertyAssertion')

    for el in objectPropertyAssertions:
        opname = getAttrVal(el[1].attrib)
        ctxname = getAttrVal(el[2].attrib)
        if opname in ops:
            if 'IRI' in el[0].attrib:
                if el[0].attrib['IRI'] == '#isImplementedOn':
                    makeOperations(opname, ctxname)
                if el[0].attrib['IRI'] == '#returns':
                    searchOps[opname]["returns"] = ctxname

    for op in list(searchOps.keys()):
        for ind in searchOps[op]["individuals"]:
            objectPropertyReturns(op, searchOps[op]["returns"], ind)


    tree.write('output.xml', pretty_print=True, xml_declaration=True)
