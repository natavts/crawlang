import rdflib
import re

def makeQuery(o,s,p):
    query = """
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

        SELECT *
        WHERE {
             """ +o+""" """ + s + """ """ + p +""".
        }
    """
    print(query)
    return g.query(query)

def makeQueryWithNetwork(o,s, p):
    query = """
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

        SELECT *
        WHERE {
             """+o+""" """ + s + """ """ + p +""".
             ?o owl:implementedFor owl:vk
        }
    """
    print(query)
    return g.query(query)
    
def getName(row):
    return re.search("<http://www.w3.org/2002/07/owl#(\w+)", row[0].n3()).group(1)

def getArr(o="?o", s="?s", p="?p", q=makeQuery):
    return [getName(e) for e in q(o=o, s=s, p=p)]
    

if __name__ == "__main__":
    g = rdflib.Graph()
    
    g.parse('generated-ontology.owl')
    

#   получаем классы нодконтекста
    nodeCtxClasses = getArr(s="rdfs:subClassOf", p="owl:NodesContext")

#    получаем инстансы нодконтекстов кроме ContextSet и ValueContext
    nodeCtxInst = [getArr(s="rdf:type", p="owl:" + i)[0] for i in nodeCtxClasses if (i != "ValueContext" and i != "ContextSet" and i != "UserInfoContext")]

    contexts = {}
    
    for c in nodeCtxInst:
        
        ops = [re.search("([A-Z][a-z0-9]+)([A-Z][a-z0-9]+)", op).group(2).lower() for op in getArr(q=makeQueryWithNetwork, s="owl:isImplementedOn", p="owl:" + c)]
        ret = [getArr(o = "owl:" + op.capitalize() + "Op", s="owl:returns")[0] for op in ops]
        ret = [r[0].upper() + r[1:] for r in ret]
        contexts[c] = list(zip(ops, ret))

    for c in list(contexts.keys()):
        CtxClassName = c[0].upper() + c[1:]
        f = open(CtxClassName+".scala", 'w')
        
#   пишем класс    
        f.write("""package itmo.escience
        
class """ + CtxClassName +"""(val awf: AWF, val previousNode: OperationTypes) extends NodesContext {""")
        
        [f.write("""
    def """ + methodAttr[0] +"""(): """ + methodAttr[1] + """ = {
        val op = new """+methodAttr[0].capitalize()+"""()
        awf.addVertex(op)
        awf.addEdge(previousNode, op)
        new """+methodAttr[1] +"""(awf, op)
      }
        """) for methodAttr in contexts[c]]
        f.write("""
    def apply(func:Function["""+ CtxClassName +""", _ <: NodesContext]) = {
    
        val extendedAwf = func(this)
        
        new """+CtxClassName+"""(extendedAwf.awf, this.previousNode)
      }
}
        """)
        f.close()
